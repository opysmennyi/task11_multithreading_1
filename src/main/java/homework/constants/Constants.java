package homework.constants;

public class Constants {
    //------FOR-MENU------------------------------
    public final static String EXIT = "Go out of here I don't wanna see you any more";
    public final static String INSERT = "Insert ";
    public final static String FOR_TASK = " for Run task ";
    public final static String MENU = "MENU: \n";
    public final static String NEW_EMPTY_LINE = "\n";
    //------FOR-TASK-ONE--------------------------
    public final static Object SYNK = new Object();
    public final static String WRITEPING = "Ping";
    public final static String WRITEPONG = "Pong";
    //------FOR-TASK-TWO--------------------------
    public final static int END_NUMBER = 13;
    public final static String SHOW_START_NUMBER = "Start number of Fibonacci numbers is - 1";
    public final static String SHOW_END_NUMBER = "End number of Fibonacci numbers is - 13";
    public final static String FIBONACCI_NUMBERS = "Fibonacci numbers are: ";
    public final static String START_TIME = "Time that program has started: ";
    //------FOR-TASK-FOUR-------------------------
    public final static String USER_END_NUMBER = "Enter end number of Fibonacci numbers: ";
    public final static String TOTAL_FIBONACCI = "The total of Fibonacci numbers are:  ";

    private Constants() {
    }

}
