package homework.task1.task.model;

import homework.constants.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MakePing {
    private static Logger LOG = LogManager.getLogger(MakePing.class);
    public Thread t2 = new Thread(() -> {
        synchronized (Constants.SYNK) {
            for (int i = 1; i <= 10; i++) {
                Constants.SYNK.notify();
                LOG.info(Constants.WRITEPING);
                try {
                    Constants.SYNK.wait();
                } catch (InterruptedException e) {
                }
            }
        }
    });
}
