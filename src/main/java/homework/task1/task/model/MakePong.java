package homework.task1.task.model;

import homework.constants.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MakePong {
    private static Logger LOG = LogManager.getLogger(MakePong.class);
    public Thread t1 = new Thread(() -> {
        synchronized (Constants.SYNK) {
            for (int i = 1; i <= 10; i++) {
                try {
                    Constants.SYNK.wait();
                    LOG.trace(Constants.WRITEPONG);
                } catch (InterruptedException e) {
                }
                Constants.SYNK.notify();
            }
        }
    });
}
