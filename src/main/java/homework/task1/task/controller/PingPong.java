package homework.task1.task.controller;

import homework.task1.task.model.MakePing;
import homework.task1.task.model.MakePong;

public class PingPong {

    public static void playPingPong() {
        MakePing makePing = new MakePing();
        MakePong makePong = new MakePong();

        makePong.t1.start();
        makePing.t2.start();
        try {
            makePong.t1.join();
            makePing.t2.join();
        } catch (InterruptedException e) {
        }
    }
}