package homework.task1.useles;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPongNotifier implements Runnable {
    private static Logger LOG = LogManager.getLogger(MakePing.class);
    private MakePing ping;
    private MakePong pong;

    public PingPongNotifier(MakePing ping, MakePong pong) {
        this.ping = ping;
        this.pong = pong;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        try {
            Thread.sleep(1000);
            synchronized (ping) {
                ping.notify();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            String name1 = Thread.currentThread().getName();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            synchronized (pong) {
                pong.notify();
            }
        }
    }
}

