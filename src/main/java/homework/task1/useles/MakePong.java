package homework.task1.useles;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MakePong {
    private static Logger LOG = LogManager.getLogger(MakePong.class);
    private String pong;

    public MakePong(String pong) {
        this.pong = pong;
    }

    public String getPong() {
        return pong;
    }

    public void setPong(String pong) {
        this.pong = pong;
    }
    //    static String PONG = "Pong";
//
//    public static void makeStringMessegePong() {
//        LOG.info(PONG);
//    }


}