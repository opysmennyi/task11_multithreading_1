package homework.task1.useles;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPongWaiter implements Runnable {

    private static Logger LOG = LogManager.getLogger(MakePing.class);

    private MakePing ping;
    private MakePong pong;

    public PingPongWaiter(MakePing ping, MakePong pong) {
        this.ping = ping;
        this.pong = pong;
    }


    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        synchronized (ping) {
            try {
                ping.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // обработаем наше сообщение
            LOG.trace(name + " : " + ping.getPing());
        }
        synchronized (pong) {
            try {
                pong.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // обработаем наше сообщение
            LOG.trace(name + " : " + pong.getPong());
        }
    }
}