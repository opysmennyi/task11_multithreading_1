package homework.task1.useles;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MakePing {
    private static Logger LOG = LogManager.getLogger(MakePing.class);
    private String ping;

    public MakePing(String ping) {
        this.ping = ping;
    }

    public String getPing() {
        return ping;
    }

    public void setPing(String ping) {
        this.ping = ping;
    }
    //    static String PING = "Ping";
//
//    public static void makeStringMessegePing() {
//        LOG.info(PING);
//    }


}