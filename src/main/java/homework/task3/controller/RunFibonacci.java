package homework.task3.controller;

import homework.constants.Constants;
import homework.task3.model.ThreadFibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class RunFibonacci {
    private static Logger LOG = LogManager.getLogger(RunFibonacci.class);

    public static void runFibonacciNumbers() {
        LOG.trace(Constants.SHOW_START_NUMBER);
        LOG.trace(Constants.SHOW_END_NUMBER);
        ThreadFibonacci.countFibonacci();
    }
}
