package homework.task3.controller;

public interface Executor {
    void execute(Runnable command);
}

