package homework.task4.model;

import homework.constants.Constants;
import homework.task4.controller.RunFibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ThreadFibonacci extends Thread {
    private static Logger LOG = LogManager.getLogger(RunFibonacci.class);
    static int endNumber;

    ThreadFibonacci() {
        this.start();
    }

    @Override
    public void run() {
        while (true) {
            threadFibonacci();
        }
    }

    public static void threadFibonacci() {
        Object sync = new Object();
        Thread thread = new Thread(() -> {
            synchronized (sync) {
                for (int quantity = 0; quantity < 5; quantity++) {
                    createAnsSumFibonacci();
                }
            }
        });
        thread.start();
    }

    public static int createAnsSumFibonacci() {
        LOG.trace(Constants.USER_END_NUMBER);
        Scanner scanner = new Scanner(System.in);
        endNumber = scanner.nextInt();
        int var1;
        int var2 = 1;
        int var3 = 1;
        int sum = 2;
        while (var3 < endNumber) {
            var1 = var2 + var3;
            var2 = var3;
            var3 = var1;
            sum = var1 + sum;
        }
        LOG.info(Constants.TOTAL_FIBONACCI + sum);
        return sum;
    }
}
