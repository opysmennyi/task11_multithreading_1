package homework.task2.task.model;

import homework.constants.Constants;
import homework.task2.task.controller.RunFibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;

public class ThreadFibonacci extends Thread {
    private static Logger LOG = LogManager.getLogger(RunFibonacci.class);

    ThreadFibonacci() {
        this.start();
    }

    @Override
    public void run() {
        while (true) {
            countFibonacci();
        }
    }

    public static void countFibonacci() {
         Object sync = new Object();
        Thread thread = new Thread(() -> { synchronized (sync){
            int var1;
            int var2 = 1;
            int var3 = 1;

        while(var3 < Constants.END_NUMBER)
            {
                var1 = var2 + var3;
                LOG.info(var1);
                var2 = var3;
                var3 = var1;
            }}});
        LOG.trace(Constants.START_TIME);
        LOG.info(LocalDateTime.now());
        LOG.trace(Constants.FIBONACCI_NUMBERS);
        thread.start();
    }
}
