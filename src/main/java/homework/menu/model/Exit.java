package homework.menu.model;

import homework.constants.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Exit {
    private static final Logger LOG = LogManager.getLogger(Exit.class);

    public static void exit() {
        LOG.trace(Constants.EXIT);
        System.exit(0);
    }
}
