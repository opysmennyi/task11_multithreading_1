//package homework.task5;
//
//import org.jetbrains.annotations.NotNull;
//
//import java.util.concurrent.*;
//
//public class ThreadSheduler extends ScheduledThreadPoolExecutor {
//
//    public ThreadSheduler(int corePoolSize) {
//        super(corePoolSize);
//    }
//
//    static class CustomTask<V> implements RunnableScheduledFuture<V> {
//        @Override
//        public boolean isPeriodic() {
//            return false;
//        }
//
//        @Override
//        public long getDelay(@NotNull TimeUnit unit) {
//            return 0;
//        }
//
//        @Override
//        public int compareTo(@NotNull Delayed o) {
//            return 0;
//        }
//
//        @Override
//        public void run() {
//
//        }
//
//        @Override
//        public boolean cancel(boolean mayInterruptIfRunning) {
//            return false;
//        }
//
//        @Override
//        public boolean isCancelled() {
//            return false;
//        }
//
//        @Override
//        public boolean isDone() {
//            return false;
//        }
//
//        @Override
//        public V get() throws InterruptedException, ExecutionException {
//            return null;
//        }
//
//        @Override
//        public V get(long timeout, @NotNull TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
//            return null;
//        }}
//
//}
//
//}
