package homework.task5;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Randomizer {

    private static Logger LOG = LogManager.getLogger(Randomizer.class);

    public static void randomNumberOfSeconds() {
        Random random = new Random();
        int rand = random.nextInt(10);
        rand += 1;
        LOG.trace(rand);
    }
}